import { useState, createContext } from 'react'

export const AppContext = createContext([{}, () => {}]);

export const ContextProvider = (props) => {
    const initialContext = {
        
        product: {
            title: '',
            price: '',
            category: '',
            rating: ''
        }
    }
    const [state, setState] = useState(initialContext)
    return (
        <AppContext.Provider value={[state, setState]}>
            {props.children}
        </AppContext.Provider>
    )
}
