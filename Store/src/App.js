import "./App.css";
import React from "react";
import Dropdown from "./pages/Dropdown";
import Home from "./pages/NotFound";
import All from "./pages/All";
import Electronics from "./pages/Electronics";
import Mens from "./pages/Mens";
import Jewelry from "./pages/Jewelry";
import Womens from "./pages/Womens";
import ProductForm from "./pages/Form";
import { Routes, BrowserRouter as Router, Route, Link } from "react-router-dom";
import Single from "./pages/Single";
import { ContextProvider } from './Context'
import 'bootstrap/dist/css/bootstrap.min.css';
import './custom.scss'
import Carousel from "./pages/Carousel";

function App() {
  return (
    <div className="App">
      <ContextProvider>
      <Router>
        <Routes>
          <Route path="/" element={<Dropdown />}></Route>
          <Route path="/all" element={<All />}></Route>
          <Route path="/electronics" element={<Electronics />}></Route>
          <Route path="/mensclothing" element={<Mens />}></Route>
          <Route path="/womensclothing" element={<Womens />}></Route>
          <Route path="/jewelery" element={<Jewelry />}></Route>
          <Route path="/products/:id" element={<Single />}></Route>
          <Route path="/carousel" element={<Carousel />}></Route>
          <Route path="/form" element={<ProductForm />}></Route>
          <Route path="*" element={<Home />}></Route>
        </Routes>
        <nav><div className="nav">
          <br></br>
          <Link className="nav-link" to="/form">
            Add new product
          </Link><br></br>
          <Link className="nav-link" to="/">
            Home
          </Link>
          </div>
        </nav>
      </Router>
      </ContextProvider>
    </div>
  );
}
export default App;
