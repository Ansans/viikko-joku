import axios from 'axios'
import { useParams } from 'react-router-dom'

const baseURL = 'https://fakestoreapi.com/products/'


export const getAllProducts = async () => {
    const result = await axios.get(`${baseURL}`)
    console.log(result.data);
    return result.data;

}

export const getCategories = async () => {
    const result = await axios.get(`${baseURL}/categories`)
    return result.data;

}

export const getElectronics = async () => {
    const result = await axios.get(`${baseURL}/category/electronics`)
    return result.data;

}

export const getMens = async () => {
    const result = await axios.get(`${baseURL}/category/men's clothing`)
    return result.data;

}

export const getWomens = async () => {
    const result = await axios.get(`${baseURL}/category/women's clothing`)
    return result.data;

}

export const getJewelry = async () => {
    const result = await axios.get(`${baseURL}/category/jewelery`)
    return result.data;

}

export const singleProduct = async (id) => {
    const result = await axios.get(`${baseURL}/${id}`)
    return result.data;

}

export const deleteProduct = async (id) => {
    const result = await axios.delete(`${baseURL}/${id}`)
    return result.status;
}

export const newProduct = async () => {
    const result = await axios.post(`${baseURL}`)
    return result.status;
}

// export const updateProduct = async () => {
//     const result = await axios.put(`${baseURL}/categories/:id`,

//     {
//         title: 'test product',
//         price: 13.5,
//         description: 'lorem ipsum set',
//         image: 'https://i.pravatar.cc',
//         category: 'electronic'
//     }
// )
// }
