import { useState, useEffect } from "react";
import { deleteProduct, singleProduct } from "../services/todoService";
import { useParams } from "react-router-dom";
import { useContext } from 'react'
import { AppContext } from '../Context'
import "./style.scss";

const Single = () => {
    const params = useParams();
    const deleteProd = deleteProduct();
  const [product, setProduct] = useState(null);
  useEffect(() => {
    const getProds = async () => {
      const data = await singleProduct(params.id);
      setProduct(data);
    };
    getProds();
  }, []);


  if (product === null) {
    return (<div>Loading...</div>)
}

const handleClick = (event, id) => {
    event.preventDefault();
    deleteProd(`/products/${id}`);
    console.log(id);
    }

  return (
    <div className="tuotteet">    
          <article key={product.id}>
            <h3 className="tuotenimi">{product.title}</h3>
            <img src={product.image} alt=""></img>
            <p className="tuote">{product.description}</p>
            <div className="res-circle">
            <div className="circle-txt">${product.price}</div>
            </div>
            <p className="rating">Rating: {product.rating.rate}</p>
            <p className="kategoria">Category: {product.category}</p>
            <button type="button" className="update">Update Product</button>
           <button type="button" onClick={e => handleClick(e, product.id)} className="delete">Delete Product</button>
           
            <br></br>
            <hr></hr>
          </article>
         
    </div>
  );
};

export default Single;
