import { useState, useEffect } from "react";
import { getJewelry } from "../services/todoService";
import { useNavigate } from "react-router-dom";
import "./style.scss";

const Jewelry = () => {
  const [products, setProducts] = useState([]);
  const navigate = useNavigate();
  useEffect(() => {
    const getProds = async () => {
      const data = await getJewelry();
      setProducts(data);
    };
    getProds();
  }, []);

  const handleClick = (event, id) => {
    event.preventDefault()
    console.log(id);
    navigate(`/products/${id}`)
 
    }

return (
  <div className="tuotteet">
    <h2>Jewelry</h2>
    {products.map((product) => {
      return (
        <article key={product.id}>
          <h3 onClick={e => handleClick(e, product.id)}  className="tuotenimi">{product.title}</h3>

            <img src={product.image} alt=""></img>
            <p className="tuote">{product.description}</p>
            <div className="res-circle">
            <div className="circle-txt">${product.price}</div>
            </div>
            <p className="rating">Rating: {product.rating.rate}</p>
            <p className="kategoria">Category: {product.category}</p>

            <br></br>
            <hr></hr>
          </article>
        );
      })}
    </div>
  );
};

export default Jewelry;
