import { useState, useEffect } from "react";
import React from "react";
import { getCategories, newProduct } from "../services/todoService";
import "./style.scss";
import Form from "react-bootstrap/Form";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import Button from "react-bootstrap/Button";

function ProductForm() {
  const [title, setTitle] = useState("");
  const [price, setPrice] = useState("");
  const [description, setDescription] = useState("");
  const [image, setImage] = useState("");
  const [category, setCategory] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    newProduct();
  };

  //     <Form>
  // <Form.Group className="mb-3" controlId="formBasicEmail">
  //   <Form.Label>Email address</Form.Label>
  //   <Form.Control type="email" placeholder="Enter email" />
  //   <Form.Text className="text-muted">
  //     We'll never share your email with anyone else.
  //   </Form.Text>
  // </Form.Group>

  return (
    <Form>
      <h2>Add a new product</h2>
      <Form.Group className="mb-3" controlId="productTitle">
        <Form.Label>
          Product title:
          <Form.Control
            type="text"
            value={title}
            onChange={(e) => setTitle(e.target.title)}
          />
        </Form.Label>
      </Form.Group>
      <Form.Group className="mb-3" controlId="productPrice">
        <Form.Label>
          Price:
          <Form.Control
            type="text"
            value={price}
            onChange={(e) => setPrice(e.target.price)}
          />
        </Form.Label>
      </Form.Group>
      <Form.Group className="mb-3" controlId="productDesc">
        <Form.Label>
          Description:
          <Form.Control
            type="text"
            value={description}
            onChange={(e) => setDescription(e.target.description)}
          />
        </Form.Label>
      </Form.Group>
      <Form.Group className="mb-3" controlId="productImage">
        <Form.Label>
          Image URL:
          <Form.Control
            type="text"
            value={image}
            onChange={(e) => setImage(e.target.image)}
          />
        </Form.Label>
      </Form.Group>
      
      <Dropdown className="mb-4">
        <Dropdown.Toggle variant="info" id="dropdown-basic">
          Select category
        </Dropdown.Toggle>
        <Dropdown.Menu> 
          <Dropdown.Item value="1">dsfs</Dropdown.Item>
          <Dropdown.Item value="2">Option 2</Dropdown.Item>
          <Dropdown.Item value="3">Option 3</Dropdown.Item>
          <Dropdown.Item value="4">Option 4</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
      <Button type="submit" onClick={handleSubmit} variant="primary">
        Submit
      </Button>{" "}
    </Form>
  );
}

const FormCat = () => {
  const [categories, setCategories] = useState([]);
  useEffect(() => {
    const getCats = async () => {
      const data = await getCategories();
      setCategories(data);
    };
    getCats();
  }, []);

  const handleSelect = (event) => {
    event.preventDefault();
    // <input type="text" value={category}
    //       onChange={(e) => setCategory(e.target.category)} />
  };

  return (
    <div className="valikko2">
      <select
        onChange={(e) => handleSelect(e)}
        name="categories"
        id="categories"
      >
        <option className="option" value="all">
          ...
        </option>
        <option className="option" value="all">
          All products
        </option>
        {categories.map((category) => {
          return (
            <option
              className="option"
              value={category}
              key={category}
              id={category}
            >
              {category}
            </option>
          );
        })}
      </select>
    </div>
  );
};

export default ProductForm;
