import { useState, useEffect } from "react";
import { getAllProducts } from "../services/todoService";
import { useNavigate } from 'react-router-dom';
import "./style.scss";
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

const All = () => {
  const [products, setProducts] = useState([]);
  const navigate = useNavigate();
  useEffect(() => {
    const getProds = async () => {
      const data = await getAllProducts();
      setProducts(data);
    
    };
    
    getProds();
  }, []);


    const handleClick = (event, id) => {
      event.preventDefault()
      console.log(id);
      navigate(`/products/${id}`)
      }

  

  return (
    <div>
      <h2>All products</h2>
      
      {products.map((product) => {
        return (
          <Container key={product.id}><Row><Col>
            <h3 onClick={e => handleClick(e, product.id)}  className="tuotenimi">{product.title}</h3>

            <img src={product.image} alt=""></img>
            <p className="tuote">{product.description}</p>
            <div className="res-circle">
            <div className="circle-txt">${product.price}</div>
            </div>
            <p className="rating">Rating: {product.rating.rate}</p>
            <p className="kategoria">Category: {product.category}</p>
            <br></br>
            <hr></hr>
            </Col></Row>
          </Container>
        );
      })}
    </div>
  );
};

export default All;
