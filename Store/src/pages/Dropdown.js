import { useState, useEffect } from "react";
import { getCategories } from "../services/todoService";
import { useNavigate } from "react-router-dom";
import "./style.scss";

const Dropdown= () => {
  const [categories, setCategories] = useState([]);
  const navigate = useNavigate();
  useEffect(() => {
    const getCats = async () => {
      const data = await getCategories();
      setCategories(data);
    };
    getCats();
  }, []);

  const handleSelect = (event) => {
    event.preventDefault();
    const path = event.target.value;
    const cleanPath = path.replace('"', "").replace(" ", "").replace("'", "");
    navigate(cleanPath);
  };

  return (
    <div className="valikko">
      <select
        onChange={(e) => handleSelect(e)}
        name="categories"
        id="categories"
      >
        <option className="option" value="all">
          ...
        </option>
        <option className="option" value="all">
          All products
        </option>
        {categories.map((category) => {
          return (
            <option
              className="option"
              value={category}
              key={category}
              id={category}
            >
              {category}
            </option>
          );
        })}
      </select>
    </div>
  );
};

export default Dropdown;
